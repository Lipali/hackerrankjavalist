import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner scan = new Scanner(System.in);
        int Number = scan.nextInt();
        ArrayList<Integer> list = new ArrayList<>();
        for (int i = 0; i < Number; i++) {
            int value = scan.nextInt();
            list.add(value);
        }
        int operation = scan.nextInt();
        for (int i = 0; i < operation; i++) {
            String action = scan.next();
            if (action.equals("Insert")) {
                int index = scan.nextInt();
                int value = scan.nextInt();
                list.add(index, value);
            }
            else  if (action.equals("Delete")){
                int index = scan.nextInt();
                list.remove(index);
            }
        }
        scan.close();


        for (Integer x : list) {
            System.out.print(x + " ");
        }
    }
}
